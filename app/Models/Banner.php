<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Banner
 *
 * @author    Haven Shen <havenshen@gmail.com>
 * @copyright    Copyright (c) Haven Shen
 */
class Banner extends Model
{
	protected $table = 'hyperlink';

	public $hyperlink1;

	public $hyperlink2;

	public $hyperlink3;

	protected $fillable = [
		'hyperlink1',
		'hyperlink2',
		'hyperlink3',
    ];
    
    public function setHyperlink1($ewanss) {
        $this->update([
			'hyper_link_1' => $ewanss
		]);
    }


}