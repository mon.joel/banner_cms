<?php

namespace App\Controllers;

use App\Models\Banner;
use App\Models\User;
use App\Controllers\Controller;
use Respect\Validation\Validator as v;
use Illuminate\Support\Facades\DB;

/**
 * HomeController
 *
 * @author    Haven Shen <havenshen@gmail.com>
 * @copyright    Copyright (c) Haven Shen
 */
class HomeController extends Controller
{
	public function index($request, $response)
	{
		if(isset($_SESSION['user'])) {
			$result = array(
				'message' => 'Upload successful',
				'code' => 200
			);

			$user = User::where('id', 1)->first();

			return $this->view->fetch('home.twig', [
				'link1' => $user['hyper_link_1'],
				'link2' => $user['hyper_link_2'],
				'link3' => $user['hyper_link_3']
			]);
		} else {
			return $this->view->render($response,'auth/signin.twig');
		}
	}

	public function saveChanges($request, $response)
	{
		try {
			$input = $request->getParsedBody();
			$uploaded_file = $request->getUploadedFiles();


			if(!($input['link1'] && $input['link2'] && $input['link3'] && $_FILES['banner1']['name'] && $_FILES['banner2']['name'] && $_FILES['banner3']['name']))
			{
				return "Invalid Form";
			} else {
				// File upload path
				$targetDir = $_SERVER['DOCUMENT_ROOT']."/manumed_cms/uploads/";
	
				for ($i = 1; $i <= 3; $i++) {
					$fileName = basename($_FILES['banner' . $i]['name']);
					$targetFilePath = $targetDir . $fileName;
					$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
	
					if(move_uploaded_file($_FILES["banner" . $i]["tmp_name"], $targetFilePath)){
						rename($targetFilePath, $targetDir . "banner" . $i . ".png"); 
					} else {
						return array(
							'message' => 'Error on Upload',
							'error' => $e,
							'code' => 400
						);
					}
				}
				
				// update DB
				$this->auth->user()->setHyperLinks($input['link1'],$input['link2'],$input['link3']);

				$result = array(
					'message' => 'Upload successful',
					'code' => 200
				);
				// return $response->withJson($result);
				$this->view->render($response,'home.twig');
			}
		} catch (Exeption $e) {
			// return $e;
		}
	}
}